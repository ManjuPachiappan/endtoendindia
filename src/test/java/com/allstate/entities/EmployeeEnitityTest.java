package com.allstate.entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EmployeeEnitityTest {

    private Employee employee;
    private LocalDate localDate;
    @BeforeEach
    void setUp() {
        localDate = LocalDate.now();
        employee = new Employee(1,"Dee", 50,"d@i.ie", 1234.00);
    }

    @Test
    void getId() {
        assertEquals(1, employee.getId());
    }


    @Test
    void getName() {
        assertEquals("Dee", employee.getName());
    }

    @Test
    void getSalary() {
        assertEquals(1234.00, employee.getSalary());
    }

    @Test
    void getEmail() { assertEquals("d@i.ie", employee.getEmail());
    }
}
