package com.allstate.entities;

import org.springframework.data.annotation.Id;

public abstract class Person {
    @Id
    private int id;
    private String name;
    private int age;

    public Person(){

    }
    public Person(int id){

        this.id=id;

    }


    public int getId(){
        return this.id;
    }
    public void setId(int id){
        this.id=id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void display(){
        System.out.printf("Values are %s,  %d", this.name,this.id);
    }
    protected void print(){
        System.out.printf("Values are %s,  %d", this.name,this.id);
    }
     void printf(){
        System.out.printf("Values are %s,  %d", this.name,this.id);
    }

    public Person(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

}
