package com.allstate.rest;

import com.allstate.entities.Employee;
import com.allstate.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class EmployeeController {
    @Autowired
    private EmployeeService service;

    @RequestMapping(value="/status",method = RequestMethod.GET)
    public String getStatus(){
        return "Employee Rest API is running";
    }

    @RequestMapping(value="/total",method = RequestMethod.GET)
    public long getTotal(){

        return service.total();
    }
    @RequestMapping(value="/all",method = RequestMethod.GET)
    public List<Employee> all(){

        return service.all();
    }
   @RequestMapping(value="/save",method = RequestMethod.POST)
    public void save(@RequestBody Employee employee){

        service.save(employee);
    }
   @RequestMapping(value="/update",method = RequestMethod.PUT)
    public long update(@RequestBody Employee employee){

        return service.update(employee);
    }

}
